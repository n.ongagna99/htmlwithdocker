1.  Git global setup


- git config --global user.name "Nolane ONGAGNA"
- git config --global user.email "n.ongagna99@gmail.com"

2. reate a new repository


- git clone git@gitlab.com:n.ongagna99/htmlwithdocker.git

- cd htmlwithdocker
- touch README.md
- git add README.md
- git commit -m "add README"
- git push -u origin master


3. Push an existing folder

- cd existing_folder
- git init
- git remote add origin git@gitlab.com:n.ongagna99/htmlwithdocker.git
- git add .
- git commit -m "Initial commit"
- git push -u origin master


4. Push an existing Git repository

- cd existing_repo
- git remote rename origin old-origin
- git remote add origin git@gitlab.com:n.ongagna99/htmlwithdocker.git
- git push -u origin --all
- git push -u origin --tags


